package selenium.scripts.ExerciseTwo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import selenium.scripts.BasicClass;

import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class EmailExercise_2 extends BasicClass {

    @Test
    public void EmailTestTwo() {
        //войти в почту
        WebElement userNameTextField = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.name("login"))));
        userNameTextField.sendKeys("testcase35");
        WebElement nextButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Ввести пароль']")));
        nextButton.click();
        WebElement passwordTextField = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("mailbox:password"))));
        passwordTextField.sendKeys("java,123");
        nextButton = driver.findElement(By.cssSelector("div [type='submit']"));
        nextButton.click();
        //assert что вход выполнен успешно
        WebElement userIcon = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("PH_user-email")));
        wait.until(ExpectedConditions.visibilityOf(userIcon));
        assertTrue(userIcon.isDisplayed());
        assertEquals(userIcon.getText(), "testcase35@mail.ru");
        //создать новое письмо
        WebElement newLetterCreationButton = wait.until(ExpectedConditions.elementToBeClickable(By.className("compose-button__wrapper")));
        newLetterCreationButton.click();
        WebElement sendToTextField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("container--H9L5q")));
        sendToTextField.sendKeys("testcase35@mail.ru");
        WebElement sendLetterTheme = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("Subject")));
        sendLetterTheme.sendKeys("мой Тест");
        WebElement sendLetterText = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("cke_editable_inline")));
        sendLetterText.sendKeys("Добрый день!");
        //отправить письмо
        WebElement sendLetterButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div span[title='Отправить']")));
        sendLetterButton.click();
        WebElement closeWindow = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div span[title='Закрыть']")));
        closeWindow.click();
        //проверить, что письмо появилось в папке отправленные
        WebElement outboxFolder = wait.until(ExpectedConditions.elementToBeClickable(By.className("ico_actions:reply")));
        outboxFolder.click();
        List<WebElement> unreadedList = wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("a.js-letter-list-item"), 10));
        System.out.println("Unreader emails in Sent folder is " + unreadedList.size());
        List<WebElement> subjectLocal = unreadedList.get(0).findElements(By.xpath("div[@class='llc__container']//*[contains(@class, 'llc__subject')]"));
        System.out.println("subject local " + subjectLocal.size());
        for (WebElement email : subjectLocal) {
            System.out.println(email.getText());
            assertEquals(email.getText(), "Self: мой Тест");
            email.click();
        }
        //проверить, что письмо появилось в папке Тест
        WebElement testFolder =  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@title,'Тест')]")));
        testFolder.click();
        List<WebElement> unreadedListTest = wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("a.js-letter-list-item"), 10));
        System.out.println("Unreader emails in Test folder is " + unreadedListTest.size());
        List<WebElement> subjectLocalTest = unreadedListTest.get(0).findElements(By.xpath("div[@class='llc__container']//*[contains(@class, 'llc__subject')]"));
        System.out.println("subject local " + subjectLocalTest.size());
        for (WebElement email : subjectLocalTest) {
            System.out.println(email.getText());
            assertEquals(email.getText(), "мой Тест");
            email.click();
        }
        //проверить контент, адресата и тему письма
        WebElement letterSubject = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("thread__subject")));
        assertTrue(letterSubject.isDisplayed());
        assertEquals(letterSubject.getText(), "мой Тест");
        System.out.println(letterSubject.getText());
        WebElement letterSender = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("letter-contact")));
        assertTrue(letterSender.isDisplayed());
        assertEquals(letterSender.getText(), "Mariia Gashkova");
        System.out.println(letterSender.getText());
        WebElement letterText = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("letter__body")));
        assertTrue(letterText.isDisplayed());
        assertTrue(letterText.getText().contains("Добрый день!"));
        System.out.println(letterText.getText());
        //выйти из учетной записи
        WebElement logOut = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("PH_logoutLink")));
        logOut.click();
    }



    @AfterMethod
    public void tearDown(){
        driver.quit();
    }
    void sleep(long timeout){
        try {
            Thread.sleep(timeout);
        }catch (InterruptedException e){
            e.printStackTrace();

        }

    }
}

