package selenium.scripts.ExerciseThree;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import selenium.scripts.BasicClass;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class EmailExercise_3 extends BasicClass {


    @Test
    public void EmailTestThree(){
        //войти в почту
        WebElement userNameTextField = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.name("login"))));
        userNameTextField.sendKeys("testcase35");
        WebElement nextButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Ввести пароль']")));
        nextButton.click();
        WebElement passwordTextField = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("mailbox:password"))));
        passwordTextField.sendKeys("java,123");
        nextButton = driver.findElement(By.cssSelector("div [type='submit']"));
        nextButton.click();
        //assert что вхож выполнен успешно
        WebElement userIcon = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("PH_user-email")));
        wait.until(ExpectedConditions.visibilityOf(userIcon));
        assertTrue(userIcon.isDisplayed());
        assertEquals(userIcon.getText(), "testcase35@mail.ru");
        //создать новое письмо
        WebElement newLetterCreationButton = wait.until(ExpectedConditions.elementToBeClickable(By.className("compose-button__wrapper")));
        newLetterCreationButton.click();
        WebElement sendToTextField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("container--H9L5q")));
        sendToTextField.sendKeys("testcase35@mail.ru");
        WebElement sendLetterTheme = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("Subject")));
        sendLetterTheme.sendKeys("Упражнение 3");
        WebElement sendLetterText = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("cke_editable_inline")));
        sendLetterText.sendKeys("Добрый день!");
        //отправить письмо
        WebElement sendLetterButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div span[title='Отправить']")));
        sendLetterButton.click();
        WebElement closeWindow = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div span[title='Закрыть']")));
        closeWindow.click();
        //проверить, что письмо появилось в папке входящие
        WebElement inboxLettersFolder = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@title,'Входящие')]")));
        inboxLettersFolder.click();
        driver.findElement(By.className("mt-snt-tmslf__subject-value")).click();
        WebElement sentLetterCheck = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div span[title='Mariia Gashkova <testcase35@mail.ru>']")));
        assertTrue(sentLetterCheck.isDisplayed());
        assertEquals(sentLetterCheck.getText(), "Mariia Gashkova");
        System.out.println(sentLetterCheck.getText());
        sentLetterCheck.click();
        //проверить контент, адресата, тему
        WebElement letterSubject = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("thread__subject")));
        assertTrue(letterSubject.isDisplayed());
        assertEquals(letterSubject.getText(), "Упражнение 3");
        System.out.println(letterSubject.getText());
        WebElement letterSender = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("letter-contact")));
        assertTrue(letterSender.isDisplayed());
        assertEquals(letterSender.getText(), "Mariia Gashkova");
        System.out.println(letterSender.getText());
        WebElement letterText = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("letter__body")));
        assertTrue(letterText.isDisplayed());
        assertTrue(letterText.getText().contains("Добрый день!"));
        System.out.println(letterText.getText());
        //удалить письмо
        WebElement deleteLetterButton = wait.until(ExpectedConditions.elementToBeClickable(By.className("ico_16-delete")));
        deleteLetterButton.click();
        //проверить, что письмо появилось в корзине
        WebElement trashFolder =  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@title,'Корзина')]")));
        trashFolder.click();
        WebElement verifyLetterBySubject = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("llc__subject")));
        System.out.println(verifyLetterBySubject.getText());
        assertEquals(verifyLetterBySubject.getText(), "Упражнение 3");
        //выйти из учетной записи
        WebElement logOut = wait.until(ExpectedConditions.elementToBeClickable(By.id("PH_logoutLink")));
        logOut.click();
        sleep(3000);
    }


    void sleep(long timeout){
        try {
            Thread.sleep(timeout);
        }catch (InterruptedException e){
            e.printStackTrace();

        }

    }
}
