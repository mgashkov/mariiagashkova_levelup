package selenium.scripts.ExerciseOne;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import selenium.scripts.BasicClass;

import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class EmailExercise_1 extends BasicClass {

    @Test
    public void EmailTestOne() {
        //войти в почту
        WebElement userNameTextField = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.name("login"))));
        userNameTextField.sendKeys("testcase35");
        WebElement nextButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Ввести пароль']")));
        nextButton.click();
        WebElement passwordTextField = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("mailbox:password"))));
        passwordTextField.sendKeys("java,123");
        nextButton = driver.findElement(By.cssSelector("div [type='submit']"));
        nextButton.click();
        // проверка, что вход выполнен успешно
        WebElement userIcon = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("PH_user-email")));
        wait.until(ExpectedConditions.visibilityOf(userIcon));
        assertTrue(userIcon.isDisplayed());
        assertEquals(userIcon.getText(), "testcase35@mail.ru");
        //создать новое письмо
        WebElement newLetterCreationButton = wait.until(ExpectedConditions.elementToBeClickable(By.className("compose-button__wrapper")));
        newLetterCreationButton.click();
        WebElement sendToTextField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("container--H9L5q")));
        sendToTextField.sendKeys("testcase35@mail.ru");
        WebElement sendLetterTheme = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("Subject")));
        sendLetterTheme.sendKeys("Упражнение 3");
        WebElement sendLetterText = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("cke_editable_inline")));
        sendLetterText.sendKeys("Добрый день!");
        //сохранить в черновиках
        WebElement saveInDraftButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div span[title='Сохранить']")));
        saveInDraftButton.click();
        WebElement closeWindowButton = wait.until(ExpectedConditions.elementToBeClickable(By.className("icon--3PdIV")));
        closeWindowButton.click();
        //проверить, что письмо сохранено в черновиках
        WebElement draftFolder = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ico_categories:drafts")));
        draftFolder.click();
        List<WebElement> unreadedList = wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("a.js-letter-list-item"), 10));
        System.out.println("Unreader emails in Test folder is " + unreadedList.size());
        List<WebElement> subjectLocal = unreadedList.get(0).findElements(By.xpath("div[@class='llc__container']//*[contains(@class, 'llc__subject')]"));
        System.out.println("subject local " + subjectLocal.size());
        for (WebElement email : subjectLocal) {
            System.out.println(email.getText());
            assertEquals(email.getText(), "Упражнение 3");
            email.click();
        }
        //проверить контент, адресата и тему письма
        WebElement letterAddressee = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("[title='testcase35@mail.ru']")));
        System.out.println("текст"+ letterAddressee.getText());
        assertEquals(letterAddressee.getText(), "testcase35@mail.ru");
        WebElement letterSubject = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("Subject")));
        System.out.println(letterSubject.getAttribute("value"));
        assertEquals(letterSubject.getAttribute("value"), "Упражнение 3");
        WebElement LetterText = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(text(),'Добрый день!')]")));
        //Отправить письмо
        WebElement button = driver.findElement(By.cssSelector("[title = 'Отправить']"));
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("arguments[0].click();", button);
        WebElement closeWindow = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div span[title='Закрыть']")));
        closeWindow.click();
        //убедиться, что письмо исчезло из черновиков
        try {
            driver.findElement(By.className("ico_categories:drafts")).click();
        } catch (StaleElementReferenceException e) {
            driver.findElement(By.className("ico_categories:drafts")).click();
        }
        try{
            driver.findElement(By.cssSelector("div span[title='testcase35@mail.ru']"));
            System.out.println("Письмо сохранилось в черновиках");
        }
        catch (NoSuchElementException e){
            System.out.println("Письмо исчезло из черновиков");
        }
        //проверить, что письмо появилось в папке отправленные
        WebElement sentLetters = wait.until(ExpectedConditions.elementToBeClickable(By.className("ico_actions:reply")));
        sentLetters.click();
        WebElement sentLetterCheck = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("div span[title='testcase35@mail.ru']"))));
        assertTrue(sentLetterCheck.isDisplayed());
        assertEquals(sentLetterCheck.getText(), "testcase35@mail.ru");
        System.out.println(sentLetterCheck.getText());
        //выйти из учетной записи
        WebElement logOut = driver.findElement(By.id("PH_logoutLink"));
        logOut.click();
    }

  /*  @AfterMethod
    public void tearDown(){
        driver.quit();
    }
    void sleep(long timeout){
        try {
            Thread.sleep(timeout);
        }catch (InterruptedException e){
            e.printStackTrace();

        }*/

    }

