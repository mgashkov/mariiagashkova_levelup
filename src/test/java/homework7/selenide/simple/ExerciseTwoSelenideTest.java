package homework7.selenide.simple;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;

public class ExerciseTwoSelenideTest {

@Test
public void ExerciseNumberTwoSelenideTest() {
open("https://mail.ru");
$("[name='login']").sendKeys("testcase35");
$(By.xpath("//*[text()='Ввести пароль']")).click();
$("[id='mailbox:password']").sendKeys("java,123");
$("[type='submit']").click();
        $("[id='PH_user-email']").shouldBe(visible);
        element("[id='PH_user-email']").shouldHave(text("testcase35@mail.ru"));
        $(".compose-button__wrapper").click();
        $(".container--H9L5q").sendKeys("testcase35@mail.ru");
        $("[name='Subject']").sendKeys("мой Тест");
        $(".cke_editable_inline").sendKeys("Добрый день!");
        $("div span[title='Отправить']").click();
        $("div span[title='Закрыть']").click();
        $(By.className("ico_actions:reply")).click();
        $$("a.js-letter-list-item").get(0).find(By.xpath("div[@class='llc__container']//*[contains(@class, 'llc__subject')]")).shouldHave(text("Self: мой Тест")).click();
        $(By.xpath("//a[contains(@title,'Тест')]")).click();
        $$("a.js-letter-list-item").get(0).find(By.xpath("div[@class='llc__container']//*[contains(@class, 'llc__subject')]")).shouldHave(text("мой Тест")).click();
        $(".thread__subject").shouldBe(visible);
        $(".thread__subject").shouldHave(text("мой Тест"));
        $(".letter-contact").shouldBe(visible);
        $(".letter-contact").shouldHave(text("Mariia Gashkova"));
        $(".letter__body").shouldBe(visible);
        $(".letter__body").shouldHave(text("Добрый день!"));
        element("[id='PH_logoutLink']").click();





}

void sleep(long timeout) {
try {
Thread.sleep(timeout);
} catch (InterruptedException e) {
e.printStackTrace();
}
}
}
