package homework7.selenide.simple;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;

        public class ExerciseThreeSelenideTest {

        @Test
        public void ExerciseNumberThreeSelenideTest() {
        open("https://mail.ru");
        $("[name='login']").sendKeys("testcase35");
        $(By.xpath("//*[text()='Ввести пароль']")).click();
        $("[id='mailbox:password']").sendKeys("java,123");
        $("[type='submit']").click();
                $("[id='PH_user-email']").shouldBe(visible);
                element("[id='PH_user-email']").shouldHave(text("testcase35@mail.ru"));
                $(".compose-button__wrapper").click();
                $(".container--H9L5q").sendKeys("testcase35@mail.ru");
                $("[name='Subject']").sendKeys("Упражнение 3");
                $(".cke_editable_inline").sendKeys("Добрый день!");
                $("div span[title='Отправить']").click();
                $("div span[title='Закрыть']").click();
                $x("//a[contains(@title,'Входящие')]").click();
                $(".mt-snt-tmslf__subject-value").click();
                $("div span[title='Mariia Gashkova <testcase35@mail.ru>']").shouldBe(visible);
                $("div span[title='Mariia Gashkova <testcase35@mail.ru>']").shouldHave(text("Mariia Gashkova"));
                $("div span[title='Mariia Gashkova <testcase35@mail.ru>']").click();
                $(".thread__subject").shouldBe(visible);
                $(".thread__subject").shouldHave(text("Упражнение 3"));
                $(".letter-contact").shouldBe(visible);
                $(".letter-contact").shouldHave(text("Mariia Gashkova"));
                $(".letter__body").shouldBe(visible);
                $(".letter__body").shouldHave(text("Добрый день!"));
                $(".ico_16-delete").click();
                $x("//a[contains(@title,'Корзина')]").click();
                $(".llc__subject").shouldBe(visible);
                $(".llc__subject").shouldHave(text("Упражнение 3"));
                element("[id='PH_logoutLink']").click();





        }

        void sleep(long timeout) {
        try {
        Thread.sleep(timeout);
        } catch (InterruptedException e) {
        e.printStackTrace();
        }
        }
        }
