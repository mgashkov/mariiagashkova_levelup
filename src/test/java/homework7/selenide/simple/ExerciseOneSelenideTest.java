package homework7.selenide.simple;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class ExerciseOneSelenideTest {
        @BeforeMethod
        public void setUp(){
                Configuration.startMaximized = true;
        }


@Test
public void ExerciseNumberTwoSelenideTest() {
        Configuration.clickViaJs=true;
        Configuration.headless = false;
        Configuration.startMaximized = true;
open("https://mail.ru");
$("[name='login']").sendKeys("testcase35");
$(By.xpath("//*[text()='Ввести пароль']")).click();
$("[id='mailbox:password']").sendKeys("java,123");
$("[type='submit']").click();
        $("[id='PH_user-email']").shouldBe(visible);
        element("[id='PH_user-email']").shouldHave(text("testcase35@mail.ru"));
        $(".compose-button__wrapper").click();
        $(".container--H9L5q").sendKeys("testcase35@mail.ru");
        $("[name='Subject']").sendKeys("Упражнение первое");
        $(".cke_editable_inline").sendKeys("Добрый день!");
        $("div span[title='Сохранить']").click();
        $(".icon--3PdIV").click();
        $(By.className("ico_categories:drafts")).click();
        $$("a.js-letter-list-item").get(0).find(By.xpath("div[@class='llc__container']//*[contains(@class, 'llc__subject')]")).shouldHave(text("Упражнение первое")).click();
        $("[title='testcase35@mail.ru']").shouldHave(text("testcase35@mail.ru"));
        $("[name='Subject']").shouldHave(value("Упражнение первое"));
        $x("//div[contains(text(),'Добрый день!')]").getText();
        Selenide.executeJavaScript("arguments[0].click();",$("[title = 'Отправить']"));
        Selenide.executeJavaScript("arguments[0].click();",$("[title = 'Закрыть']"));
        $(By.className("ico_actions:reply")).click();
        $$("a.js-letter-list-item").get(0).find(By.xpath("div[@class='llc__container']//*[contains(@class, 'llc__subject')]")).shouldHave(text("Упражнение первое")).click();
        element("[id='PH_logoutLink']").click();






}

void sleep(long timeout) {
try {
Thread.sleep(timeout);
} catch (InterruptedException e) {
e.printStackTrace();
}
}
}
