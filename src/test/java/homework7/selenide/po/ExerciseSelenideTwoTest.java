package homework7.selenide.po;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import homework7.selenide.po.seleniumstyle.EmailPage;
import homework7.selenide.po.seleniumstyle.HomePage;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.text;
import static homework7.selenide.po.ConfProperties.getProperty;

public class ExerciseSelenideTwoTest {
    @Test
    public void EmailExerciseTwoTest() {
        Configuration.startMaximized = true;
        String password = getProperty("password");
        String username = getProperty("username");
        new HomePage()
                .open()
                .login(username,password)
                .userButtonMethod()
                .shouldHave(text("testcase35@mail.ru"));
        new EmailPage()
                .createNewLetter("testcase35@mail.ru", "мой Тест", "Добрый день!")
                .sendLetter()
                .outboxFolderOpen()
                .findLetterBySubject("мой Тест")
                .testFolderOpen()
                .findLetterBySubject("мой Тест")
                .subjectOfLetterInReceivedMethod()
                .shouldHave(text("мой Тест"));
        new EmailPage()
             .textOfLetterInReceivedMethod()
                .shouldHave(text("Добрый день!"));
        new EmailPage()
                .senderOfLetterInReceivedMethod()
                .shouldHave(text("Mariia Gashkova"));
        new EmailPage()
                .exitButtonClick()
                .userNameTextField()
                .shouldBe(Condition.visible);
    }
}

