package homework7.selenide.po;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import homework7.selenide.po.seleniumstyle.EmailPage;
import homework7.selenide.po.seleniumstyle.HomePage;
import io.qameta.allure.selenide.AllureSelenide;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.text;
import static homework7.selenide.po.ConfProperties.getProperty;

public class ExerciseSelenideOneTest {
    @BeforeSuite
    public void beforeSuite(){
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().savePageSource(true).screenshots(true));
        Configuration.headless=true;
    }
    @Test
    public void EmailExerciseOneTest() {
        Configuration.startMaximized = true;
        String password = getProperty("password");
        String username = getProperty("username");
        new HomePage()
                .open()
                .login(username,password)
                .userButtonMethod()
                .shouldHave(text("testcase35@mail.ru"));
        new EmailPage()
                .createNewLetter("testcase35@mail.ru", "Упражнение 1", "Добрый день!")
                .saveLetter()
                .draftFolderOpen()
                .findLetterBySubject("Упражнение 1")
                .subjectOfLetterInDraftsMethod()
                .shouldHave(attribute("value","Упражнение 1"));
        new EmailPage()
             .textOfLetterInDraftsMethod()
                .shouldHave(text("Добрый день!"));
        new EmailPage()
                .senderOfLetterInDraftsdMethod()
                .shouldHave(text("testcase35@mail.ru"));
        new EmailPage()
                .sendLetterFromDrafts()
                .outboxFolderOpen()
                .findLetterBySubject("Упражнение 1")
                .exitButtonClick()
                .userNameTextField()
                .shouldBe(Condition.visible);
    }
}

