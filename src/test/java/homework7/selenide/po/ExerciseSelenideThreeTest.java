package homework7.selenide.po;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import homework7.selenide.po.seleniumstyle.EmailPage;
import homework7.selenide.po.seleniumstyle.HomePage;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.text;
import static homework7.selenide.po.ConfProperties.getProperty;

public class ExerciseSelenideThreeTest {
    @Test
    public void EmailExerciseThreeTest() {
        Configuration.startMaximized = true;
        String password = getProperty("password");
        String username = getProperty("username");
        new HomePage()
                .open()
                .login(username,password)
                .userButtonMethod()
                .shouldHave(text("testcase35@mail.ru"));
        new EmailPage()
                .createNewLetter("testcase35@mail.ru", "Упражнение 3", "Добрый день!")
                .sendLetter()
                .inboxFolderOpen()
                .findLetterBySubject("Упражнение 3")
                .subjectOfLetterInReceivedMethod()
                .shouldHave(text("Упражнение 3"));
        new EmailPage()
             .textOfLetterInReceivedMethod()
        .shouldHave(text("Добрый день!"));
        new EmailPage()
                .senderOfLetterInReceivedMethod()
                .shouldHave(text("Mariia Gashkova"));
        new EmailPage()
                .deleteLetter()
                .trashFolderOpen()
                .findLetterBySubject("Упражнение 3")
                .exitButtonClick()
                .userNameTextField()
                .shouldBe(Condition.visible);
    }
}

