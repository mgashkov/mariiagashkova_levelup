package fluent;


import org.testng.annotations.Test;

import static fluent.ConfProperties.getProperty;
import static org.testng.Assert.assertEquals;

public class EmailExercise_1 extends AbstractBaseTest {
    @Test
    public void EmailExerciseOneTest() {
        String password = getProperty("password");
        String username = getProperty("username");
        String displayedUserName = new HomePage(driver)
        .open()
        .login(username,password)
        .getTextUserButton();
        assertEquals(displayedUserName, "testcase35@mail.ru");
        String subjectOfReceivedLetterInbox = new EmailPage(driver)
                .createNewLetter("testcase35@mail.ru", "Упражнение 3", "Добрый день!")
                .saveLetter()
                .draftFolderOpen()
                .findLetterBySubject()
                .checkSubjectOfLetterInDrafts();
        assertEquals(subjectOfReceivedLetterInbox,"Упражнение 3");
        EmailPage sendLetter = new EmailPage(driver)
                .sendLetter();

    }
}

