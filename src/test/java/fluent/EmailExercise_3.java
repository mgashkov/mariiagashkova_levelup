package fluent;


import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class EmailExercise_3 extends AbstractBaseTest {
    @Test
    public void EmailExerciseThreeTest() {
        String displayedUserName = new HomePage(driver)
        .open()
        .login("testcase35","java,123")
        .getTextUserButton();
        assertEquals(displayedUserName, "testcase35@mail.ru");
        String subjectOfReceivedLetterInbox = new EmailPage(driver)
                .createNewLetter("testcase35@mail.ru", "Упражнение 3", "Добрый день!")
        .sendLetter()
                .inboxFolderOpen()
        .findLetterBySubject()
                .checkSubjectOfLetterInReceived();
        assertEquals(subjectOfReceivedLetterInbox,"Упражнение 3");
        String textOfReceivedLetterInbox = new EmailPage(driver)
                .checkTextOfLetterInReceived();
        assertEquals(textOfReceivedLetterInbox, "Добрый день!");
        String senderOfReceivedLetterInbox = new EmailPage(driver)
                .checkSenderOfLetterInReceived();
        assertEquals(senderOfReceivedLetterInbox,"Mariia Gashkova" );
        boolean enterButtonDisplayed = new EmailPage(driver)
                .deleteLetter()
                .trashFolderOpen()
                .findLetterBySubject()
        .exitButtonClick()
                .isUserNameTextFieldDisplayed();
        assertTrue(enterButtonDisplayed);

    }
}

