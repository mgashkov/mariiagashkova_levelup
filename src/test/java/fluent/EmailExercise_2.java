package fluent;


import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class EmailExercise_2 extends AbstractBaseTest {
    @Test
    public void EmailExerciseTwoTest() {
        String displayedUserName = new HomePage(driver)
                .open()
                .login("testcase35","java,123")
                .getTextUserButton();
        assertEquals(displayedUserName, "testcase35@mail.ru");
        String subjectOfReceivedLetterInbox = new EmailPage(driver)
                .createNewLetter("testcase35@mail.ru", "мой Тест", "Добрый день!")
                .sendLetter()
                .outboxFolderOpen()
                .findLetterBySubject()
                .testFolderOpen()
                .findLetterBySubject()
                .checkSubjectOfLetterInReceived();
        assertEquals(subjectOfReceivedLetterInbox,"мой Тест");
        String textOfReceivedLetterInbox = new EmailPage(driver)
                .checkTextOfLetterInReceived();
        assertEquals(textOfReceivedLetterInbox, "Добрый день!");
        String senderOfReceivedLetterInbox = new EmailPage(driver)
                .checkSenderOfLetterInReceived();
        assertEquals(senderOfReceivedLetterInbox,"Mariia Gashkova" );
        boolean enterButtonDisplayed = new EmailPage(driver)
                .exitButtonClick()
                .isUserNameTextFieldDisplayed();
        assertTrue(enterButtonDisplayed);
    }
}

