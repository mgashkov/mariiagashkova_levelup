package homework1.task1;

import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) {
        Operations operations = new Operations();
        operations.Welcome();
        System.out.println("введите число: " );
        Scanner userNumber = new Scanner(System.in);
        double firstNumber;
        double secondNumber;
        firstNumber = userNumber.nextDouble();
        int operator;
        System.out.println("Выберите метод: 0-сложение, 1 - вычитание, 2 -умножение, 3- возведение в степень, 4 -факториал, 5 - фиабоначи");
        operator = userNumber.nextInt();
        if (operator == 0){
            Add plus = new Add();
            System.out.println("введите второе число" );
            secondNumber = userNumber.nextDouble();
            System.out.println("Результат сложения: " + plus.getAdd(firstNumber,secondNumber));
        }
        else if (operator == 1) {
            Substract minus = new Substract();
            System.out.println("введите второе число" );
            secondNumber = userNumber.nextDouble();
            System.out.println("Результат Вычитания: "+ minus.getSubtract((int)firstNumber,(int) secondNumber));

        }
        else if (operator == 2) {
            Multiply multiply = new Multiply();
            System.out.println("введите множитель" );
            secondNumber = userNumber.nextDouble();
            System.out.println("Результат умножения: "+ multiply.getMultiply(firstNumber,secondNumber));
        }
        else if (operator == 3) {
            Power stepen = new Power();
            System.out.println("введите степень" );
            secondNumber = userNumber.nextDouble();
            System.out.println("Результат возведения "+ (int)firstNumber + " в степень " + (int)secondNumber + ": " + stepen.getPower((int)firstNumber,(int)secondNumber));
        }
        else if (operator == 4) {
            Factorial factorial = new Factorial();
            System.out.println("Результатом вычисления факториала числа " + firstNumber + " является: " + factorial.getFactorial((int)firstNumber));
        }
        else if (operator == 5) {
            Fiabonachi fiabonachi = new Fiabonachi();
            System.out.println("Результат значения числа фиабоначи: "+ fiabonachi.getFiabonachi((int)firstNumber));
        }
        else
            System.out.println("Выберите один из вариантов : 0,1,2,3,4,5");

    }

}
