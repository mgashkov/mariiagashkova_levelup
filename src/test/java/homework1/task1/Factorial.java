package homework1.task1;

public class Factorial {
    public double getFactorial(int n) {
        double result = 1;
        for (int i = 1; i <= n; i++) {
            result = result * i;
        }
        return result;
    }
}
