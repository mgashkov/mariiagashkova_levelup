package homework1.task1;

public class Power {
    public double getPower(double a, double b) {
        double answer = a;

        for (int x = 2; x <= b; x++) {
            answer *= a;
        }

        return answer;
    }
}
