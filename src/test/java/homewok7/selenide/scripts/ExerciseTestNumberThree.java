package homewok7.selenide.scripts;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class ExerciseTestNumberThree {
    @Test
    public void NewExerciseNumberThree(){
        open("https://mail.ru");
        $("[name='login']").sendKeys("testcase35");
        $(By.xpath("//*[text()='Ввести пароль']")).click();
        $(By.id("mailbox:password")).sendKeys("java,123");
        $("div [type='submit']").click();
    }
}
