package fluent;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class HomePage extends AbstractBasePage {
    private static final String URL = "https://mail.ru/";
    @FindBy(name = "login")
    private WebElement usernameTextField;
    @FindBy(xpath = "//*[text()='Ввести пароль']")
    private WebElement enterPasswordButton;
    @FindBy(id = "mailbox:password")
    private WebElement passwordTextField;
    @FindBy(css = "div [type='submit']")
    private WebElement enterEmailButton;
    /*private WebElement userButton;
    private WebElement authorisationButton;*/
    public HomePage(WebDriver driver) {
        super(driver);
    }
    public HomePage open(){
        driver.get(URL);
        return this;
    }
    public boolean isUserNameTextFieldDisplayed(){
        return wait.until(visibilityOf(usernameTextField)).isDisplayed();
    }

    public EmailPage login(String username, String password) {
        sendKeysToElement(usernameTextField,username);
        elementClick(enterPasswordButton);
        sendKeysToElement(passwordTextField,password);
        enterEmailButton.click();
        return new EmailPage(driver);
    }
}
