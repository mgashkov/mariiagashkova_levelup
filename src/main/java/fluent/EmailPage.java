package fluent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;


public class EmailPage extends AbstractBasePage {
    @FindBy(id = "PH_logoutLink")
    private WebElement exitButton;
    @FindBy(className = "compose-button__wrapper")
    private WebElement createNewLetterButton;
    @FindBy(className = "container--H9L5q")
    private WebElement addresseeTextField;
    @FindBy(name = "Subject")
    private WebElement letterSubjectTextField;
    @FindBy(className = "cke_editable_inline")
    private WebElement letterTextField;
    @FindBy(css = "div span[title='Отправить']")
    private WebElement sendLetterButton;
    @FindBy(css = "div span[title='Закрыть']")
    private WebElement closeWindowButton;
    @FindBy(xpath = "//a[contains(@title,'Входящие')]")
    private WebElement inboxFolderButton;
    @FindBy(className = "mt-snt-tmslf__subject-value")
    private WebElement lettersToYourselfButton;
    @FindBy(css = "a.js-letter-list-item")
    private List<WebElement> unreadList;
    @FindBy(css = "(div span[title='Mariia Gashkova <testcase35@mail.ru>'])")
    private WebElement sentLetterInInbox;
    private List<WebElement> subjectLocalTest;
    @FindBy(xpath = "(//a[contains(@title,'Письма себе')]")
    private WebElement lettersToYouselfFolder;
    @FindBy(className = "ico_actions:reply")
    private WebElement outboxFolderOpenButton;
    @FindBy(className = "ico_16-delete")
    private WebElement deleteLetterButton;
    @FindBy(xpath = "//a[contains(@title,'Корзина')]")
    private WebElement trashFolderOpenButton;
    @FindBy(xpath = "//a[contains(@title,'Тест')]")
    private WebElement testFolderOpenButton;
    @FindBy(css = "div span[title='Сохранить']")
    private WebElement saveLetterButton;
    @FindBy(className = "icon--3PdIV")
    private WebElement closeAfterSaveLetterButton;
    @FindBy (className = "ico_categories:drafts")
    private WebElement draftFolderOpenButton;





   /* public boolean isSentLetterDisplayed(){
        return wait.until(visibilityOf(sentLetterInInbox)).isDisplayed();
    }
    public String getTextSentLetterSender(){
        return wait.until(visibilityOf(sentLetterInInbox)).getText();
    }
    public EmailPage sentLetterOpen(){
        elementClick(sentLetterInInbox);
        return new EmailPage(driver);
    }*/

    public EmailPage(WebDriver driver) {
        super(driver);
    }

    public String getTextUserButton() {

        return wait.until(visibilityOfElementLocated(By.id("PH_user-email"))).getText();
    }
    public String checkSubjectOfLetterInReceived(){
        return wait.until(visibilityOfElementLocated(By.className("thread__subject"))).getText();
    }

    public String checkTextOfLetterInReceived(){
        return wait.until(visibilityOfElementLocated(By.className("letter__body"))).getText();
    }
    public String checkSenderOfLetterInReceived(){
        return wait.until(visibilityOfElementLocated(By.className("letter-contact"))).getText();
    }
    public String checkSubjectOfLetterInDrafts(){
        return driver.findElement(By.name("Subject")).getAttribute("value");
    }



    public HomePage exitButtonClick() {
        elementClick(exitButton);
        return new HomePage(driver);
    }

    public EmailPage deleteLetter(){
        elementClick(deleteLetterButton);
        return new EmailPage(driver);
    }
    public EmailPage saveLetter(){
        elementClick(saveLetterButton);
        elementClick(closeAfterSaveLetterButton);
        return new EmailPage(driver);
    }

    public EmailPage createNewLetter(String addresse, String subject, String letterText) {
        elementClick(createNewLetterButton);
        sendKeysToElement(addresseeTextField, addresse);
        sendKeysToElement(letterSubjectTextField, subject);
        sendKeysToElement(letterTextField, letterText);
        return new EmailPage(driver);
    }

    public EmailPage sendLetter() {
        /*elementClick(sendLetterButton);*/
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("arguments[0].click();", sendLetterButton);

        elementClick(closeWindowButton);
        return new EmailPage(driver);
    }

    public EmailPage inboxFolderOpen() {
        /*elementClick(inboxFolderButton);
        elementClick(lettersToYourselfButton);*/
        driver.findElement(By.xpath("(//a[contains(@title,'Письма себе')])")).click();
        return this;
    }
    public EmailPage trashFolderOpen(){
        elementClick(trashFolderOpenButton);
        return this;
    }

    public EmailPage outboxFolderOpen() {
        elementClick(outboxFolderOpenButton);
        return new EmailPage(driver);
    }

    public EmailPage testFolderOpen() {
        elementClick(testFolderOpenButton);
        return new EmailPage(driver);
    }
    public EmailPage draftFolderOpen() {
        elementClick(draftFolderOpenButton);
        return new EmailPage(driver);
    }




    public EmailPage findLetterBySubject() {
       /* elementClick(outboxFolderOpenButton);*/
        List<WebElement> unreadedList = wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("a.js-letter-list-item"), 1));
        List<WebElement> subjectLocalTest = unreadList.get(0).findElements(By.xpath("div[@class='llc__container']//*[contains(@class, 'llc__subject')]"));
        for (WebElement email : subjectLocalTest) {
            email.click();
            System.out.println(email.getText());
        }

        return new EmailPage(driver);
    }
}

