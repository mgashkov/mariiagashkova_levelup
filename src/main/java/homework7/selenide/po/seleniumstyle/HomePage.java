package homework7.selenide.po.seleniumstyle;


import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.page;

public class HomePage {
    private static final String URL = "https://mail.ru/";
    @FindBy(name = "login")
    private SelenideElement usernameTextField;
    @FindBy(xpath = "//*[text()='Ввести пароль']")
    private SelenideElement enterPasswordButton;
    @FindBy(id = "mailbox:password")
    private SelenideElement passwordTextField;
    @FindBy(css = "div [type='submit']")
    private SelenideElement enterEmailButton;
    /*private WebElement userButton;
    private WebElement authorisationButton;*/

  @Step("Открывается домашняя страница")
    public HomePage open(){
        Selenide.open(URL);
       return page (this);
    }

    public SelenideElement userNameTextField()
    {
        return usernameTextField;
    }

    @Step("Залогиниться")

    public EmailPage login(String username, String password) {
        usernameTextField.sendKeys(username);
        enterPasswordButton.click();
        passwordTextField.sendKeys(password);
        enterEmailButton.click();
        return page (EmailPage.class);
    }
}

