package homework7.selenide.po.seleniumstyle;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;


public class EmailPage {
    @FindBy(id = "PH_logoutLink")
    private SelenideElement exitButton;
    @FindBy(className = "compose-button__wrapper")
    private SelenideElement createNewLetterButton;
    @FindBy(className = "container--H9L5q")
    private SelenideElement addresseeTextField;
    @FindBy(name = "Subject")
    private SelenideElement letterSubjectTextField;
    @FindBy(className = "cke_editable_inline")
    private SelenideElement letterTextField;
    @FindBy(css = "div span[title='Отправить']")
    private SelenideElement sendLetterButton;
    @FindBy(css = "div span[title='Закрыть']")
    private SelenideElement closeWindowButton;
    @FindBy(xpath = "//a[contains(@title,'Входящие')]")
    private SelenideElement inboxFolderButton;
    @FindBy(className = "mt-snt-tmslf__subject-value")
    private SelenideElement lettersToYourselfButton;
    @FindBy(css = "a.js-letter-list-item")
    private ElementsCollection unreadList;
    @FindBy(css = "(div span[title='Mariia Gashkova <testcase35@mail.ru>'])")
    private SelenideElement sentLetterInInbox;
    private ElementsCollection subjectLocalTest;
    @FindBy(xpath = "(//a[contains(@title,'Письма себе')]")
    private SelenideElement lettersToYouselfFolder;
    @FindBy(className = "ico_actions:reply")
    private SelenideElement outboxFolderOpenButton;
    @FindBy(className = "ico_16-delete")
    private SelenideElement deleteLetterButton;
    @FindBy(xpath = "//a[contains(@title,'Корзина')]")
    private SelenideElement trashFolderOpenButton;
    @FindBy(xpath = "//a[contains(@title,'Тест')]")
    private SelenideElement testFolderOpenButton;
    @FindBy(css = "div span[title='Сохранить']")
    private SelenideElement saveLetterButton;
    @FindBy(className = "icon--3PdIV")
    private SelenideElement closeAfterSaveLetterButton;
    @FindBy(className = "ico_categories:drafts")
    private SelenideElement draftFolderOpenButton;
    @FindBy (id = "PH_user-email")
    private SelenideElement userButton;
    @FindBy(className = "thread__subject")
    private SelenideElement subjectOfLetterInReceived;
    @FindBy(className = "letter__body")
    private SelenideElement textOfLetterInReceived;
    @FindBy(className = "letter-contact")
    private SelenideElement senderOfLetterInReceived;
    @FindBy(name = "Subject")
    private SelenideElement subjectOfLetterInDrafts;
    @FindBy(xpath = "(//a[contains(@title,'Письма себе')])")
    private SelenideElement inboxFolderOpenButton;
    @FindBy(className = "js-readmsg-msg")
    private SelenideElement textOfLetterInDrafts;
    @FindBy(css = "[title='testcase35@mail.ru']")
    private SelenideElement senderOfLetterInDrafts;


    public EmailPage(){
        page(this);
    }



    public SelenideElement userButtonMethod() {
        return userButton;
    }

    public SelenideElement subjectOfLetterInReceivedMethod() {
        return subjectOfLetterInReceived;
    }

    public SelenideElement textOfLetterInReceivedMethod() {
        return textOfLetterInReceived;
    }

    public SelenideElement textOfLetterInDraftsMethod(){
       return textOfLetterInDrafts;
    }

    public SelenideElement senderOfLetterInReceivedMethod() {
        return senderOfLetterInReceived;
    }
    public SelenideElement senderOfLetterInDraftsdMethod() {
        return senderOfLetterInDrafts;
    }

    public SelenideElement subjectOfLetterInDraftsMethod() {
        return subjectOfLetterInDrafts;
    }
    /*.getAttribute("value"*/

    @Step("выход из учетной записи")
    public HomePage exitButtonClick() {
        exitButton.click();
        return page(HomePage.class);
    }
    @Step("удаление письма")
    public EmailPage deleteLetter() {
        deleteLetterButton.click();
        return page(this);
    }

    @Step("сохрание письма")
    public EmailPage saveLetter() {
        saveLetterButton.click();
        closeAfterSaveLetterButton.click();
        return page(this);
    }
    @Step("создание нового письма")
    public EmailPage createNewLetter(String addresse, String subject, String letterText) {
        createNewLetterButton.click();
        addresseeTextField.sendKeys(addresse);
        letterSubjectTextField.sendKeys(subject);
        letterTextField.sendKeys(letterText);
        return page(EmailPage.class);
    }
    @Step("отправление письма")
    public EmailPage sendLetter() {
        /*elementClick(sendLetterButton);*/
        Selenide.executeJavaScript("arguments[0].click();",$("[title = 'Отправить']"));
        closeWindowButton.click();
        return page(this);
    }
    @Step("отправление письма из черновиков")
    public EmailPage sendLetterFromDrafts() {
        Selenide.executeJavaScript("arguments[0].click();",$("[title = 'Отправить']"));
        Selenide.executeJavaScript("arguments[0].click();",$("[title = 'Закрыть']"));
        $(By.className("ico_actions:reply")).click();
        return page(this);
    }
    @Step("открытие папки Входящие")
    public EmailPage inboxFolderOpen() {
        /*elementClick(inboxFolderButton);
        elementClick(lettersToYourselfButton);*/
        inboxFolderOpenButton.click();
        /*driver.findElement(By.xpath("(//a[contains(@title,'Письма себе')])")).click();*/
        return page(this);
    }
    @Step("открытие папки Корзина")
    public EmailPage trashFolderOpen() {
        trashFolderOpenButton.click();
        return page(this);
    }
    @Step("открытие папки Исходящие")
    public EmailPage outboxFolderOpen() {
        outboxFolderOpenButton.click();
        return page(this);
    }
    @Step("открытие папки Тест")
    public EmailPage testFolderOpen() {
        testFolderOpenButton.click();
        return page(this);
    }
    @Step("открытие папки черновики")
    public EmailPage draftFolderOpen() {
        draftFolderOpenButton.click();
        return page(this);
    }

    @Step("поиск письма по теме")
    public EmailPage findLetterBySubject(String text) {
        /* elementClick(outboxFolderOpenButton);*/
        $$("a.js-letter-list-item").get(0).find(By.xpath("div[@class='llc__container']//*[contains(@class, 'llc__subject')]")).shouldHave(text(text)).click();
        return page(this);
    }
}


