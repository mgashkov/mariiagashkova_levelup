package homework3.task2;

import java.util.*;

import static java.util.Collections.shuffle;

public class CollectionOfNumberSequence {
    public static void main(String[] args) {
        NumberSequenceDemonstration WelcomeAndExplain = new NumberSequenceDemonstration();
        WelcomeAndExplain.ShowTheMeaningOfTheProgram();
        ArrayList<Integer> NumberSequenceList = new ArrayList<>(); // Создание коллекции
        for (int i = 0; i < 10000; i++) { // Заполнение коллекции последовательностью чисел от 0 до 10000
            NumberSequenceList.add(i);
        }
        Set<Integer> NumberSequenceSet = new HashSet<>(NumberSequenceList); // проверка на уникальность
        if (NumberSequenceList.size() == NumberSequenceSet.size()) {
            System.out.println("Коллекция содержит только уникальные значения");
        } else {
            System.out.println("Коллекция содержит повторяещиеся значения");
        }


        shuffle(NumberSequenceList);
        System.out.println("Первые 10 чисел списка: ");
        for (int i = 0; i < 10; i++) {
            System.out.println(NumberSequenceList.get(i) + " "); // порядок произвольный, выводим первые 10 чисел для демонстрации
        }

        ArrayList<Integer> divideByTwo = new ArrayList<>();

        for (int i = 0; i < NumberSequenceList.size(); i++) {
            if (NumberSequenceList.get(i) % 2 == 0) {
                divideByTwo.add(NumberSequenceList.get(i)); // Сохраняем числа с делителем 2 в первый список
            }
        }
        ArrayList<Integer> divideByThree = new ArrayList<>();

        for (int i = 0; i < NumberSequenceList.size(); i++) {
            if (NumberSequenceList.get(i) % 3 == 0) {
                divideByThree.add(NumberSequenceList.get(i)); // Сохраняем числа с делителем 3 во второй список
            }
        }

        ArrayList<Integer> divideByFive = new ArrayList<>();

        for (int i = 0; i < NumberSequenceList.size(); i++) {
            if (NumberSequenceList.get(i) % 5 == 0) {
                divideByFive.add(NumberSequenceList.get(i)); // Сохраняем числа с делителем 5 в третий список
            }
        }

        ArrayList<Integer> divideBySeven = new ArrayList<>();

        for (int i = 0; i < NumberSequenceList.size(); i++) {
            if (NumberSequenceList.get(i) % 7 == 0) {
                divideBySeven.add(NumberSequenceList.get(i)); // Сохраняем числа с делителем 7 в четвертый список
            }
        }
        Map<Integer, List<Integer>> map = new HashMap<>(); // Добавляем в коллекцию для возможности доступа по делителю
        map.put(3, divideByThree);
        map.put(5, divideByFive);
        map.put(7, divideBySeven);
        map.put(2, divideByTwo);
        for (Map.Entry<Integer, List<Integer>> entry : map.entrySet()) {
            Integer key = entry.getKey();
            List<Integer> values = entry.getValue();
        }
        System.out.println("Элементы, которые делятся на 2 =" + map.get(2));
        System.out.println("Элементы, которые делятся на 3 =" + map.get(3));
        System.out.println("Элементы, которые делятся на 5 =" + map.get(5));
        System.out.println("Элементы, которые делятся на 7 =" + map.get(7));
    }
}
