package homework2.task2;

import homework2.task2.AirPlane.CivilAirplane.Hydroplane;
import homework2.task2.AirPlane.Plane;
import homework2.task2.ExceptionExamples.CapacityException;
import homework2.task2.ExceptionExamples.LoadNotEnoughException;
import homework2.task2.ExceptionExamples.TotalLoadException;
import homework2.task2.Helicopter.Helicopter;
import homework2.task2.Helicopter.Quadcopter;

public class TestAircompany {
    public static void main(String[] args) {
        Aircraft boeng = new Plane(100,500,190,"Boeng");
        Aircraft jet = new Plane(3,300,290,"Jet");
        Aircraft mi8 = new Helicopter(100, 300,500,"Mi8");
        Aircraft superquadr = new Quadcopter(100,300,700, "Superquadr");
        Aircraft hydrofly = new Hydroplane(100,4000,2000,"Hydrofly");
        Aircompany Aeroflot = new Aircompany();
        Aeroflot.addAircraft(boeng);
        Aeroflot.addAircraft(jet);
        Aeroflot.addAircraft(mi8);
        Aeroflot.addAircraft(superquadr);
        Aeroflot.addAircraft(hydrofly);
        hydrofly.ShowAircraftInfo();
        mi8.ShowAircraftInfo();
        boeng.ShowAircraftInfo();
        superquadr.ShowAircraftInfo();
        System.out.println("Общая вместимость летных средств в компании: ");
        try {
            Aeroflot.getTotalCapacity();
        } catch (CapacityException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Общая грузоподъемность летных средств в компании: ");
        try {
            Aeroflot.getTotalLoad();
        } catch (TotalLoadException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Сортировка по дальности полета: ");
        Aeroflot.sortByFlightLength();
        System.out.println("Поиск летных средств, у котрых load и capacity меньше заданного значения");
        try {
            System.out.println(Aeroflot.findByCriterion(4,100));
        } catch (LoadNotEnoughException e) {
            System.out.println(e.getMessage());
        }
    }
}

