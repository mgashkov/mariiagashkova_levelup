package homework2.task2.AirPlane.CivilAirplane;

import homework2.task2.Transportable;

public class PassengerJet extends CivilAirplane implements Transportable {
    public PassengerJet(int capacity, int load, int flightLength, String name) {
        super(capacity, load, flightLength, name);
    }
    @Override
    public void ShowAircraftInfo(){
        System.out.println("Пассажирский самолет с названием: " + getName() + ", вместимостью: " + getCapacity() + ", грузоподъемностью: "+ getLoad() + ", дальностью полета: " + getFlightLength());
    }

    @Override
    public void transportPassengers() {
        System.out.println("Самолет перевозит пассажиров");
    }
}
