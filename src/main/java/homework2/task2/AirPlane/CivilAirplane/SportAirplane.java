package homework2.task2.AirPlane.CivilAirplane;

public class SportAirplane extends CivilAirplane {
    public SportAirplane(int capacity, int load, int flightLength, String name) {
        super(capacity, load, flightLength, name);
    }
    @Override
    public void ShowAircraftInfo() {
        System.out.println("Cпортивный самолет с названием: " + getName() + ", вместимостью: " + getCapacity() + ", грузоподъемностью: " + getLoad() + ", дальностью полета: " + getFlightLength());
    }
}
