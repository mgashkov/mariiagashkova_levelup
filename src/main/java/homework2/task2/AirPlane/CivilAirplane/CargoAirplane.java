package homework2.task2.AirPlane.CivilAirplane;


public class CargoAirplane extends CivilAirplane {
    public CargoAirplane(int capacity, int load, int flightLength, String name) {
        super(capacity, load, flightLength, name);
    }
    @Override
    public void ShowAircraftInfo(){
        System.out.println("Грузовой самолет с названием: " + getName() + ", вместимостью: " + getCapacity() + ", грузоподъемностью: "+ getLoad() + ", дальностью полета: " + getFlightLength());
    }
}