package homework2.task2.AirPlane.CivilAirplane;

    public class Hydroplane extends CivilAirplane {
        public Hydroplane(int capacity, int load, int flightLength, String name) {
            super(capacity, load, flightLength, name);
        }
        public void ShowAircraftInfo(){
            System.out.println("Гидро самолет с названием: " + getName() + ", вместимостью: " + getCapacity() + ", грузоподъемностью: "+ getLoad() + ", дальностью полета: " + getFlightLength());
        }
    }

