package homework2.task2.AirPlane.CivilAirplane;

import homework2.task2.AirPlane.Plane;

public class CivilAirplane extends Plane {
    CivilAirplane(int capacity, int load, int flightLength, String name) {
        super(capacity, load, flightLength, name);
    }
    @Override
    public void ShowAircraftInfo(){
        System.out.println("Гражданский самолет с названием: " + getName() + ", вместимостью: " + getCapacity() + ", грузоподъемностью: "+ getLoad() + ", дальностью полета: " + getFlightLength());
    }
}
