package homework2.task2.AirPlane.MilitaryAirplane;

import homework2.task2.AirPlane.Plane;
import homework2.task2.Fightable;

public class MilitaryAirplane extends Plane implements Fightable {
    MilitaryAirplane(int capacity, int load, int flightLength, String name) {
        super(capacity, load, flightLength, name);
    }
    @Override
    public void ShowAircraftInfo() {
        System.out.println("Военный самолет с названием: " + getName() + ", вместимостью: " + getCapacity() + ", грузоподъемностью: " + getLoad() + ", дальностью полета: " + getFlightLength());
    }

    @Override
    public void fight() {
        System.out.println("Самолет оснащен боевой техникой");
    }
}
