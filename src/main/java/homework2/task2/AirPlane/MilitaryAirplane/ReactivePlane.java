package homework2.task2.AirPlane.MilitaryAirplane;

public class ReactivePlane extends MilitaryAirplane {
    public ReactivePlane(int capacity, int load, int flightLength, String name) {
        super(capacity, load, flightLength, name);
    }

    @Override
    public void ShowAircraftInfo() {
        System.out.println("Реактивный самолет с названием: " + getName() + ", вместимостью: " + getCapacity() + ", грузоподъемностью: " + getLoad() + ", дальностью полета: " + getFlightLength());
    }
}
