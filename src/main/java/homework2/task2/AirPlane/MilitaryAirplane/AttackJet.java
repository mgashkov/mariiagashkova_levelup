package homework2.task2.AirPlane.MilitaryAirplane;

import homework2.task2.Fightable;

public class AttackJet extends MilitaryAirplane {
    public AttackJet(int capacity, int load, int flightLength, String name) {
        super(capacity, load, flightLength, name);
    }
    @Override
    public void ShowAircraftInfo() {
        System.out.println("Самолет истребитель с названием: " + getName() + ", вместимостью: " + getCapacity() + ", грузоподъемностью: "+ getLoad() + ", дальностью полета: " + getFlightLength());
    }
}
