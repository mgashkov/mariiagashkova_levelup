package homework2.task2.ExceptionExamples;

public class CapacityException extends Exception{
    public CapacityException() {
    }

    public CapacityException(String message) {
        super(message);
    }
}
