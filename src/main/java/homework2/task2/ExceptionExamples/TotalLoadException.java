package homework2.task2.ExceptionExamples;

public class TotalLoadException extends Exception {
    public TotalLoadException() {
    }

    public TotalLoadException(String message) {
        super(message);
    }
}
