package homework2.task2.ExceptionExamples;

public class LoadNotEnoughException extends Exception {

    public LoadNotEnoughException(String message) {
        super(message);
    }

    public LoadNotEnoughException() {
    }
}
