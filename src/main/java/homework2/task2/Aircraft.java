package homework2.task2;

import java.util.Collections;
import java.util.Objects;

public abstract class Aircraft implements Comparable<Aircraft>{
    private int capacity;
    private int load;
    private int flightLength;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Aircraft(int capacity, int load, int flightLength, String name) {
        this.capacity = capacity;
        this.load = load;
        this.flightLength = flightLength;
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getLoad() {
        return load;
    }

    public void setLoad(int load) {
        this.load = load;
    }

    public int getFlightLength() {
        return flightLength;
    }

    public void setFlightLength(int flightLength) {
        this.flightLength = flightLength;
    }
    public abstract void ShowAircraftInfo();


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Aircraft aircraft = (Aircraft) o;
        return capacity == aircraft.capacity &&
                load == aircraft.load &&
                flightLength == aircraft.flightLength &&
                Objects.equals(name, aircraft.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(capacity, load, flightLength, name);
    }

    @Override
    public int compareTo(Aircraft o) {
        return this.flightLength - o.flightLength;
    }

    @Override
    public String toString() {
        return "Aircraft{" +
                "capacity=" + capacity +
                ", load=" + load +
                ", flightLength=" + flightLength +
                ", name='" + name + '\'' +
                '}';
    }
}

