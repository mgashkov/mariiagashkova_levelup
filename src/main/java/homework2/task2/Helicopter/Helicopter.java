package homework2.task2.Helicopter;

import homework2.task2.Aircraft;

public class Helicopter extends Aircraft {

     public Helicopter(int capacity, int load, int flightLength, String name) {
        super(capacity, load, flightLength, name);
    }
    @Override
    public void ShowAircraftInfo(){
        System.out.println("Вертолет с названием: " + getName() + ", вместимостью: " + getCapacity() + ", грузоподъемностью: "+ getLoad() + ", дальностью полета: " + getFlightLength());
    }

}

