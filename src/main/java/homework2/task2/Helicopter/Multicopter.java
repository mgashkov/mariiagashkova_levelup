package homework2.task2.Helicopter;

public class Multicopter extends Helicopter {
    public Multicopter(int capacity, int load, int flightLength, String name) {
        super(capacity, load, flightLength, name);
    }
    @Override
    public void ShowAircraftInfo(){
        System.out.println("Мультикоптер с названием: " + getName() + ", вместимостью: " + getCapacity() + ", грузоподъемностью: "+ getLoad() + ", дальностью полета: " + getFlightLength());
    }
}
