package homework2.task2;

import homework2.task2.ExceptionExamples.CapacityException;
import homework2.task2.ExceptionExamples.LoadNotEnoughException;
import homework2.task2.ExceptionExamples.TotalLoadException;

import java.util.ArrayList;
import java.util.List;

public class Aircompany {
    private String name;
    private List<Aircraft> aircrafts = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Aircraft> getAircrafts() {
        return aircrafts;
    }

    public void setAircrafts(List<Aircraft> aircrafts) {
        this.aircrafts = aircrafts;
    }

    void addAircraft(Aircraft aircraft) {
        aircrafts.add(aircraft);
    }

    void getTotalCapacity() throws CapacityException {
        int totalCapacity = 0;
        for (Aircraft aircraft : aircrafts) {
            totalCapacity += aircraft.getCapacity();
            if (aircraft.getCapacity() < 5) {
                throw new CapacityException("ВНИМАНИЕ!!! Вместимость одного из летных средств недопустимо мала! Необходима проверка параметра!!");
            }
            System.out.println("totalCapacity :" + totalCapacity);
        }
    }

    void getTotalLoad() throws TotalLoadException {
        int totalLoad = 0;
        for (Aircraft aircraft : aircrafts) {
            totalLoad += aircraft.getLoad();
        }
        if (totalLoad >5000){
            throw new TotalLoadException("ВНИМАНИЕ!!!Общая грузоподъемность вышла за допустимые значения! Пожалуйста,сообщите об этом руководству компании");
        }
        System.out.println("totalLoad :" + totalLoad);
    }

     void sortByFlightLength(){
        for (Aircraft aircraft : aircrafts){
            System.out.println(aircraft);
        }

    }
     List <Aircraft> findByCriterion (int load, int capacity) throws LoadNotEnoughException {
        if (load <5){
            throw new LoadNotEnoughException("ВНИМАНИЕ!!!Поиск не может быть выполнен! Заданная грузоподъемность недостаточна, таких самолетов не существует");
        }
        List <Aircraft> result = new ArrayList<>();
        for (Aircraft aircraft : aircrafts) {
            if(aircraft.getLoad() < load  && aircraft.getCapacity()< capacity )
                result.add(aircraft);
        }
        return  result;
    }
}

